#include <ArduinoJson.h>
#include <math.h>
#include <EEPROM.h>
#include "Streaming.h"
#include "io_pins.h"
#include "RGBLed.h"
#include "ColorSensor.h"
#include "Colorimeter.h"
#include "EEPROMAnything.h"
#include "SerialReceiver.h"
#include "SerialHandler.h"
#include "tests.h"

// To upload this firmware configure the board to "Arduino Nano" and the processor to "ATMega328p (Old Bootloader)".

Colorimeter colorimeter;
SerialHandler comm;

void setup() {
    Serial.begin(9600);
    colorimeter.initialize();
    
}


void loop() {
  // comm.processInput();
  
  parse_json_from_serial();

  // To echo serial back to the sender, uncomment the following:
  //if (Serial.available()) {        // If anything comes in Serial (USB),
  //  Serial.write(Serial.read());  // read it and send it out Serial1 (pins 0 & 1)
  //}
}


// Methods
// ----------------------------------------------------------------------------

// Deserialize a JSON document with ArduinoJson.
// https://arduinojson.org/v6/example/parser/
void parse_json_from_serial() {
  // Check if the computer is transmitting.
  while(Serial.available()) {
    
    // Skip newline characters.
    int next_byte = Serial.peek();
    if(next_byte == 10 || next_byte == 13){
      // Remove the NL/CR byte from the serial buffer.
      Serial.read();
    
    // Parse JSON and call methods.
    } else {
      // Allocate the JSON document for the incoming message.
      // This one must be bigger than the sender's because it must store the strings.
      //
      // Inside the brackets, 200 is the RAM allocated to this document.
      // Don't forget to change this value to match your requirement.
      // Use https://arduinojson.org/v6/assistant to compute the capacity.
      StaticJsonDocument<300> doc;

      // StaticJsonObject allocates memory on the stack, it can be
      // replaced by DynamicJsonDocument which allocates in the heap.
      //
      // DynamicJsonDocument  doc(200);

      // Read the JSON document from the "link" serial port.
      // Note: JSON reading terminates when a valid JSON is received, not considering line breaks.
      // Adding them to the message will result in incomplete/empty errors from deserializeJson.
      DeserializationError err = deserializeJson(doc, Serial);

      // Allocate the JSON document for the response.
      StaticJsonDocument<200> response_doc;

      // Check if parsing weresponse_docnt well.
      if (err == DeserializationError::Ok){
        
        // If so, get the method's name.
        String method = doc["method"] | "none";


        
        // Insert the ID or a default one if it is missing.
        bool has_id = doc.containsKey("id"); // true
        if(!has_id){
          response_doc["id"] = (int)-1;
        } else {
          response_doc["id"] = doc["id"];
        }

        // Then route the contents to the appropriate local function.
        if (method == "none"){
          response_doc["message"] = "No method selected";
        
        // CalibrateS method.
        //General calibrate 
        } else if (method == "calibrate"){
          
          colorimeter.calibrate();
          if (colorimeter.checkCalibration() ) {
              colorimeter.EEPROM_saveCalibration();
              response_doc["message"] = "ok";
          } else {
              response_doc["message"] = "error";
          }
        // Red calibrate
        } else if (method == "calibrate_red"){    
     
      colorimeter.calibrateRed();
          if (colorimeter.checkCalibrationRed()) {
           colorimeter.EEPROM_saveCalibrationRed();
              response_doc["message"] = "ok";
          } else {
              response_doc["message"] = "error";
          }
    
      

        //Green calibrate
        } else if (method == "calibrate_green"){
    colorimeter.calibrateGreen();
    if (colorimeter.checkCalibrationGreen()) {
        colorimeter.EEPROM_saveCalibrationGreen();
            response_doc["message"] = "ok";
          } else {
              response_doc["message"] = "error";
          }

        //Blue calibrate
        } else if (method == "calibrate_blue"){
        colorimeter.calibrateBlue();
       if (colorimeter.checkCalibrationBlue()) {
        colorimeter.EEPROM_saveCalibrationBlue();
        response_doc["message"] = "ok";
          } else {
              response_doc["message"] = "error";
          }

        } else if (method == "calibrate_white"){
          colorimeter.calibrateWhite();
    if (colorimeter.checkCalibrationWhite()) {
        colorimeter.EEPROM_saveCalibrationWhite();
        response_doc["message"] = "ok";
          } else {
              response_doc["message"] = "error";
          }

       
       
        // Get freq method
        } else if (method == "freq_red"){
        
        if (colorimeter.getFrequencyRed()){
          colorimeter.absorbance.red;
                   response_doc["f_red"] = colorimeter.frequency.red;
          } else {
              response_doc["message"] = "error";
        } colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        } else if (method == "freq_blue"){
      
        if (colorimeter.getFrequencyBlue()){
          colorimeter.absorbance.red;
                   response_doc["freq_blue"] = (float) colorimeter.frequency.blue;
          } else {
              response_doc["message"] = "error";
        } colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        } else if (method == "freq_green"){
      
        if (colorimeter.getFrequencyGreen()){
          colorimeter.absorbance.red;
                   response_doc["freq_green"] = (float) colorimeter.frequency.green;
          } else {
              response_doc["message"] = "error";
        } colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        } else if (method == "freq_white"){
               
               if (colorimeter.getFrequencyWhite()){
          colorimeter.absorbance.red;
                   response_doc["freq_white"] = (float) colorimeter.frequency.white;
                  
                   
          } else {
              response_doc["message"] = "error";
        }colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
                

        //Get measurement method

        } else if (method == "data_red"){
        colorimeter.getMeasurementRed();
        if (colorimeter.transmission.red){
         //colorimeter.absorbance.red;
                   response_doc["data_red"] = (float) colorimeter.transmission.red;
          } else {
              response_doc["message"] = "error";
        } colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        } else if (method == "data_blue"){
       colorimeter.getMeasurementBlue();
        if (colorimeter.transmission.blue){
          //colorimeter.absorbance.blue;
                   response_doc["data_blue"] = (float) colorimeter.transmission.blue;
          } else {
              response_doc["message"] = "error";
        } colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        } else if (method == "data_green"){
       colorimeter.getMeasurementGreen();
        if (colorimeter.transmission.green){
          //colorimeter.absorbance.green;
                   response_doc["data_green"] = (float) colorimeter.transmission.green;
          } else {
              response_doc["message"] = "error";
        } colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        } else if (method == "data_white"){
                colorimeter.getMeasurementWhite();
               if (colorimeter.transmission.white){
          //colorimeter.absorbance.white;
                   response_doc["data_white"] = (float) colorimeter.transmission.white;
                  
                   
          } else {
              response_doc["message"] = "error";
        }colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
                
        // Get all data method
        } else if (method == "rgbw_data"){
                colorimeter.getMeasurementWhite();
                colorimeter.getMeasurementRed();
                colorimeter.getMeasurementBlue();
                colorimeter.getMeasurementGreen();
               if (colorimeter.transmission.white){
          //colorimeter.absorbance.white;
        response_doc["rgbw_data"]["W"] = colorimeter.transmission.white;
        response_doc["rgbw_data"]["R"] = colorimeter.transmission.red;
        response_doc["rgbw_data"]["G"] = colorimeter.transmission.green;
        response_doc["rgbw_data"]["B"] = colorimeter.transmission.blue;
                   
          } else {
              response_doc["message"] = "error";
        }colorimeter.sensor.setChannelClear();
        colorimeter.led.setOff();
        }

        // Generate the minified JSON and send it to the Serial port.
        serializeJson(response_doc, Serial);
        
      } else {
        // If parsing failed, respond with a JSON document with information.
        String message = "Deserialization error: ";
        message = message + err.c_str();
        response_doc["error"] = message;
        // Send response
        serializeJson(response_doc, Serial);
        // Start a new line.
        //SerialUSB.println();

        // Flush all bytes in the "link" serial port buffer.
        while (Serial.available() > 0){
          Serial.read();
          // Serial.println(Serial.read());
        }
      }
    }
    // Parse the next message.
    }}  
