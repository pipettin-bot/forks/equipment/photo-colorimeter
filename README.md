## Fotocolorímetro open source

Adaptación del colorimetro de CoSensores:https://gitlab.com/cosensores/color-metro

Este dispositivo emplea un protocolo de comunicación [JSON-RPC](https://arduinojson.org/) . 

![foto](/3D prints/20231213_115612.jpg)
![foto](/3D prints/20231213_115736.jpg)
![foto](/3D prints/20231213_115839.jpg)

Diagrama de conexion: 
![foto](/Electronics/esquematico.png)
